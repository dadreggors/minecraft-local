FROM ubuntu:22.04
MAINTAINER David Dreggors “dadreggors@gmail.com”
RUN apt-get update \
    && apt install -y openjdk-17-jdk-headless \
    && apt install -y libc6-x32 libc6-i386 wget curl python3 python3-pip \
    && useradd -ms /bin/bash --home-dir /minecraft minecraft
USER minecraft
ENV PATH=$PATH:/minecraft/.local/bin
RUN python3 -m pip install --upgrade pip \
    && pip install mcstatus --user
WORKDIR /minecraft
EXPOSE 25565/tcp
ENTRYPOINT "./start.sh"
